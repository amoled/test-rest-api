<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = ['question', 'answer'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function path()
    {
        return '/questions/' . $this->id;
    }
}
